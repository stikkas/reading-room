import {Controller, Get} from 'routing-controllers';

@Controller('/')
export class HelloWorld {
    @Get('/')
    async get(): Promise<any> {
        return {msg: 'INDEX'};
    }
}
