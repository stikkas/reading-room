'use strict';
import {Application} from './config/application';
import config from 'config';

console.log(config.get('rr.db.port'));
console.log(config.get('rr.db.host'));
console.log(config.get('rr.db.name'));

new Application(3000).start();
