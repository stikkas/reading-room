import {ExpressConfig} from './express';

export class Application {
    server: any;
    express: ExpressConfig;
    private readonly port: number;

    constructor(port: number) {
        this.express = new ExpressConfig();
        this.port = port;

    }

    start() {
        this.server = this.express.app.listen(this.port, () => {
            console.log(`Server Started! Express: http://localhost:${this.port}`);
        });
    }
}
