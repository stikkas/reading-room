import * as bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import * as path from 'path';
import {useExpressServer} from 'routing-controllers';

export class ExpressConfig {
    app: express.Express;

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
        this.setUpControllers();
    }

    public setUpControllers() {
        const controllersPath = path.resolve('dist', 'controllers');
        useExpressServer(this.app, {
                controllers: [controllersPath + '/*.controller.js'],
            },
        );
    }
}
